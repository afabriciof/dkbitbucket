import telnetlib
import socket
import sys
import traceback

from dkutils.validation import validate_globals

TELNET = telnetlib.Telnet()


def run_checker():
    result = {}
    result['dkhost'] = check(DKHOST_URL, int(HTTPS_PORT))
    result['vault'] = check(VAULT_URL, int(VAULT_PORT))
    result['atlas0'] = check(ATLAS0_URL, int(ATLAS_PORT))
    result['atlas1'] = check(ATLAS1_URL, int(ATLAS_PORT))
    result['atlas2'] = check(ATLAS2_URL, int(ATLAS_PORT))
    result['awsses'] = check(AWSSES_URL, int(HTTPS_PORT))
    result['dkghe'] = check(DKGHE_URL, int(HTTPS_PORT))
    result['dockerhub'] = check(DOCKERHUB_URL, int(HTTPS_PORT))
    result['k8s'] = check(K8S_URL, int(K8S_PORT))
    result['vpn'] = check_udp(VPN_URL, int(VPN_PORT))
    result['vpnhttps'] = check_udp(VPN_URL, int(HTTPS_PORT))
    LOGGER.info('-----------------------------')
    return result


def check_udp(url, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        LOGGER.info('-----------------------------')
        LOGGER.info('check %s %s' % (url, port))
        sock.connect((url, port))
        LOGGER.info('works')
        sock.close()
        return 0
    except:
        LOGGER.error('failed')
        return 1


def check(url, port):
    try:
        LOGGER.info('-----------------------------')
        LOGGER.info('check %s %s' % (url, port))
        TELNET.open(url, port, 2)
        TELNET.close()
        LOGGER.info('works')
        return 0
    except:
        LOGGER.error('failed')
        return 1


try:
    success = False
    validate_globals([
        'DKHOST_URL', 
        'VAULT_URL',
        'ATLAS0_URL',
        'ATLAS1_URL',
        'ATLAS2_URL',
        'AWSSES_URL', 
        'DKGHE_URL',
        'DOCKERHUB_URL',
        'K8S_URL',
        'VPN_URL',
        'HTTPS_PORT',
        'VAULT_PORT',
        'ATLAS_PORT',
        'K8S_PORT',
        'VPN_PORT',
    ])

    result = run_checker()
    dkhost = result['dkhost']
    vault = result['vault']
    atlas0 = result['atlas0']
    atlas1 = result['atlas1']
    atlas2 = result['atlas2']
    awsses = result['awsses']
    dkghe = result['dkghe']
    dockerhub = result['dockerhub']
    k8s = result['k8s']
    vpn = result['vpn']
    vpnhttps = result['vpnhttps']
    success = not any(result.values())
except Exception as e:
    LOGGER.error(f'DataKitchen Agent health check failed:\n{traceback.format_exc()}')
