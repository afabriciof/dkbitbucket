* DataKitchen Web App:    https://cloud.datakitchen.io
* DKCloudCommand:         DataKitchen’s command line interface
* DataKitchen Help:       https://datakitchen.readme.io/docs
* DataKitchen Website:    https://datakitchen.io/
* DataKitchen Blog:       https://blog.datakitchen.io/blog
* DataOps Manifesto:      https://www.dataopsmanifesto.org
* DKCC on PyPI:           https://pypi.org/project/DKCloudCommand/

