SET search_path TO {{redshiftConfig.schema}};

CREATE TABLE {{load_data_s3_to_redshift_node.table_name}}
(
  Row_ID           INTEGER NOT NULL,
  Order_ID         VARCHAR(100),
  Order_Date       DATE,
  Ship_Date        DATE,
  Ship_Mode        VARCHAR(100),
  Customer_ID      VARCHAR(100),
  Customer_Name    VARCHAR(100),
  Segment          VARCHAR(100),
  City             VARCHAR(100),
  State            VARCHAR(100),
  Country          VARCHAR(100),
  Postal_Code      CHAR(5),
  Market           VARCHAR(100),
  Region           VARCHAR(100),
  Product_ID       VARCHAR(100),
  Category         VARCHAR(100),
  Sub_Category     VARCHAR(100),
  Product_Name     VARCHAR(1000),
  Sales            NUMERIC(14,3),
  Quantity         INTEGER,
  Discount         NUMERIC(14,3),
  Profit           NUMERIC(14,3),
  Shipping_Cost    NUMERIC(14,3),
  Order_Priority   VARCHAR(100)
);

copy {{load_data_s3_to_redshift_node.table_name}}
from 's3://{{s3Config.bucket}}/{{load_data_s3_to_redshift_node.wildcard_prefix_s3}}'
credentials 'aws_iam_role=arn:aws:iam::{{ s3Config.copy_role }}'
TRIMBLANKS delimiter '{{load_data_s3_to_redshift_node.delimiter}}' DATEFORMAT AS '{{load_data_s3_to_redshift_node.date_format}}';

SELECT COUNT(*) FROM {{load_data_s3_to_redshift_node.table_name}};