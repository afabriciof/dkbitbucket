SET search_path TO {{redshiftConfig.schema}};

CREATE TABLE demo_superstore_sales
(
  Row_ID           INTEGER NOT NULL,
  Order_ID         VARCHAR(100),
  Order_Date       DATE,
  Ship_Date        DATE,
  Ship_Mode        VARCHAR(100),
  Customer_ID      VARCHAR(100),
  Customer_Name    VARCHAR(100),
  Segment          VARCHAR(100),
  City             VARCHAR(100),
  State            VARCHAR(100),
  Country          VARCHAR(100),
  Postal_Code      CHAR(5),
  Market           VARCHAR(100),
  Region           VARCHAR(100),
  Product_ID       VARCHAR(100),
  Category         VARCHAR(100),
  Sub_Category     VARCHAR(100),
  Product_Name     VARCHAR(1000),
  Sales            NUMERIC(14,3),
  Quantity         INTEGER,
  Discount         NUMERIC(14,3),
  Profit           NUMERIC(14,3),
  Shipping_Cost    NUMERIC(14,3),
  Order_Priority   VARCHAR(100)
);

SELECT COUNT(*) FROM demo_superstore_sales;