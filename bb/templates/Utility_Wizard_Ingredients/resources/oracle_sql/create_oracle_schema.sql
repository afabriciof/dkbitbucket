
-- DROP USER IF EXISTS
BEGIN
      EXECUTE IMMEDIATE 'DROP USER {{oracleConfig.schema}} CASCADE';
EXCEPTION
      WHEN OTHERS THEN NULL;
END;
/


-- CREATE NEW USER
CREATE USER {{oracleConfig.schema}} IDENTIFIED BY {{oracleConfig.password}}
/


-- ADD PRIVILIGES TO NEW USER
ALTER user {{oracleConfig.schema}} QUOTA unlimited on USERS
/

GRANT UNLIMITED TABLESPACE TO {{oracleConfig.schema}}
/


GRANT create session TO {{oracleConfig.schema}}
/

GRANT create table TO {{oracleConfig.schema}}
/

GRANT create view TO {{oracleConfig.schema}}
/

GRANT create any trigger TO {{oracleConfig.schema}}
/

GRANT create any procedure TO {{oracleConfig.schema}}
/

GRANT create sequence TO {{oracleConfig.schema}}
/

GRANT create synonym TO {{oracleConfig.schema}}
/

SELECT count(username) FROM dba_users WHERE lower(username) = '{{oracleConfig.schema}}'