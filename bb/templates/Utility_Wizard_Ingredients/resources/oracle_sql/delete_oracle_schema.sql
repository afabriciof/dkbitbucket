

-- DROP USER IF EXISTS
BEGIN
      EXECUTE IMMEDIATE 'DROP USER {{oracleConfig.schema}} CASCADE';
EXCEPTION
      WHEN OTHERS THEN NULL;
  END;
/


SELECT count(username)
FROM dba_users WHERE lower(username) = '{{oracleConfig.schema}}'