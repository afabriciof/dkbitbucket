from google.cloud import bigquery
from google.oauth2 import service_account
from google.cloud.exceptions import NotFound

global LOGGER

global dataset_name
global project_id

def bq_delete_dataset():
    key_path = 'docker-share/keyfile.json'
    credentials = service_account.Credentials.from_service_account_file(
        key_path,
        scopes=["https://www.googleapis.com/auth/cloud-platform"],
    )

    client = bigquery.Client(
        credentials=credentials,
        project=credentials.project_id,
    )
    dataset_ref = client.dataset(dataset_name)

    try:
        client.get_dataset(dataset_ref)
        LOGGER.info('Dataset creation commencing.')
        dataset = bigquery.Dataset(dataset_ref)
        dataset = client.delete_dataset(dataset)
        LOGGER.info('Dataset ' + dataset_name + ' deleted.')


    except NotFound:
        LOGGER.info('The ' + dataset_name + ' does not exist so need not be deleted.')

    finally:
        LOGGER.info('Dataset deletion process complete.')


if __name__ == '__main__':
    bq_delete_dataset()