from google.cloud import bigquery
from google.oauth2 import service_account
from google.cloud.exceptions import NotFound

global LOGGER

global dataset_name
global project_id

def bq_create_dataset():
    key_path = 'docker-share/keyfile.json'
    credentials = service_account.Credentials.from_service_account_file(
        key_path,
        scopes=["https://www.googleapis.com/auth/cloud-platform"],
    )

    client = bigquery.Client(
        credentials=credentials,
        project=credentials.project_id,
    )
    dataset_ref = client.dataset(dataset_name)

    try:
        client.get_dataset(dataset_ref)
        LOGGER.info('The ' + dataset_name + ' BigQuery Dataset already exists.')

    except NotFound:
        LOGGER.info('Dataset creation commencing.')
        dataset = bigquery.Dataset(dataset_ref)
        dataset.description = 'Created by DataKitchen for use by the ' + dataset_name + ' Kitchen.'
        dataset = client.create_dataset(dataset)
        LOGGER.info('Dataset ' + dataset_name + ' created.')
    finally:
        LOGGER.info('Dataset creation process complete.')


if __name__ == '__main__':
    bq_create_dataset()