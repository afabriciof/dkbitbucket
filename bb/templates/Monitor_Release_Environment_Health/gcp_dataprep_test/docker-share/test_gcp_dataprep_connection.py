import os
import requests

global LOGGER, success


URL = os.environ["url"]
TOKEN = os.environ["token"]
HEADERS = {'Authorization': 'Bearer {}'.format(TOKEN)}

response = requests.get(URL + '/v4/jobGroups/{}'.format(4074007), headers=HEADERS)
success = response.status_code == 200
LOGGER.info("Google Cloud Dataprep response: {}".format(response.json()))
