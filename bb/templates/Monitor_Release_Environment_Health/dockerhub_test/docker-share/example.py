import time

global LOGGER
global sleep_time


if __name__ == '__main__':

    LOGGER.info('THE {{CurrentVariation}} VARIATION OF THE {{RecipeName}} RECIPE IS PRESENTLY RUNNING IN THE {{CurrentKitchen}} KITCHEN with ORDER ID {{CurrentOrderId}} AND ORDERRUN ID {{CurrentOrderRunId}}.')

    LOGGER.info('THE RUN WILL NOW SLEEP FOR '+ str(sleep_time) + ' SECONDS.')

    time.sleep(int(sleep_time))

    LOGGER.info('SLEEP COMPLETE. CONTINUING ORDERRUN PROCESSING NOW.')

    exported_sleep_time = sleep_time
