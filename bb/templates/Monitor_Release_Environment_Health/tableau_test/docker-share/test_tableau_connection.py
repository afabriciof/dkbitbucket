import os

import tableauserverclient as TSC

global LOGGER


def main():

    # define variables
    USERNAME = os.environ["tableau_username"]
    PASSWORD = os.environ["tableau_password"]
    SITENAME = os.environ["tableau_sitename"]
    SERVER = os.environ["tableau_server"]


    # create an auth object
    tableau_auth = TSC.TableauAuth(USERNAME, PASSWORD, SITENAME)
    
	# create an instance for your server
    server = TSC.Server(SERVER)
    
	# sign in to the tableau server
    server.auth.sign_in(tableau_auth)

    # Log tableau server version
    LOGGER.info("Tableau Server Version: " + server.version)


if __name__ == '__main__':
    main()