import logging

from azure.storage.blob import BlobServiceClient
from dkutils.validation import validate_globals

global BLOB_CONTAINER, CONNECTION_STRING, LOGGER, container_exists

if 'LOGGER' not in globals():
    LOGGER = logging.getLogger()
    LOGGER.addHandler(logging.StreamHandler())
    LOGGER.setLevel(logging.INFO)
container_exists = False
validate_globals(['BLOB_CONTAINER', 'CONNECTION_STRING'])
LOGGER.info(f"CONNECTION: {CONNECTION_STRING}")
LOGGER.info('Testing connection to Azure Blob Storage Container...')
blob_service_client = BlobServiceClient.from_connection_string(CONNECTION_STRING)
for container in blob_service_client.list_containers(include_metadata=True):
    LOGGER.info(f"Container: {container['name']}")
    if container['name'] == BLOB_CONTAINER:
        container_exists = True
        break
LOGGER.info('Successful Connection: {}'.format(container_exists))
LOGGER.info('Finished testing connection to Azure Blob Storage Container...')
