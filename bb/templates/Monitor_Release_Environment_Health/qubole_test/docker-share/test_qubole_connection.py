from qds_sdk.qubole import Qubole

global LOGGER, success


# Validate input paramters
if not api_token:
    raise Exception("Undefined API token")

if not api_url:
    raise Exception("Undefined API url")

if not api_test_path:
    raise Exception("Undefined API test path")

success = False

# Connect to Qubole
Qubole.configure(api_token=api_token, api_url=api_url)
agent = Qubole.agent()
response = agent.get(api_test_path)
if 'error' in response:
	raise Exception('Failed to connect to Qubole: {}'.format(response['error']))
	
LOGGER.info("Successfully connected to Qubole. Account details: {}".format(response))

# If we make it here, declare success
success = True