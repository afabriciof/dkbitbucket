import logging
import traceback

from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.datafactory import DataFactoryManagementClient
from dkutils.validation import validate_globals


try:
    success = False
    validate_globals([
        'AZURE_CLIENT_ID', 
        'AZURE_CLIENT_SECRET', 
        'AZURE_DATA_FACTORY_NAME', 
        'AZURE_RESOURCE_GROUP_NAME', 
        'AZURE_SUBSCRIPTION_ID',
        'AZURE_TENANT_ID',
    ])
    credentials = ServicePrincipalCredentials(
        client_id=AZURE_CLIENT_ID,
        secret=AZURE_CLIENT_SECRET,
        tenant=AZURE_TENANT_ID
    )
    client = DataFactoryManagementClient(credentials, AZURE_SUBSCRIPTION_ID)
    df = client.factories.get(AZURE_RESOURCE_GROUP_NAME, AZURE_DATA_FACTORY_NAME)
    success = True
except Exception as e:
    LOGGER.error(f'Trigger failure:\n{traceback.format_exc()}')
